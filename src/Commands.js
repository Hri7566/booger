const DiscordClient = require("./DiscordClient");
const Color = require('./Color');
const crypto = require('crypto');
const fs = require('fs');

module.exports = (bot) => {
    bot.addCommand = async (cmd, minargs, func, hidden) => {
        if (typeof cmd !== 'object') cmd = [cmd];
        bot.commands.set(cmd, {
            cmd: cmd,
            minargs: typeof minargs == 'number' ? minargs : 0,
            func: typeof func == 'function' ? func : () => {},
            hidden: typeof hidden == 'boolean' ? hidden : false
        });
    }

    var fish = ["Angelfish", "Arapaima", "Arowana", "Barbel Steed", "Barred Knifejaw", "Bitterling", "Black Bass", "Blowfish", "Blue Marlin", "Bluegill", "Brook Trout", "Butterflyfish", "Can", "Carp", "Catfish", "Char", "Cherry Salmon", "Clownfish", "Coelacanth", "Crawfish", "Crucian Carp", "Dab", "Dace", "Dorado", "Eel", "Football fish", "Freshwater Goby", "Frog", "Gar", "Giant Snakehead", "Giant Trevally", "Goldfish", "Guppy", "Hammerhead Shark", "Horse Mackerel", "Jellyfish", "Key", "Killifish", "King Salmon", "Koi", "Large Bass", "Loach", "Lobster", "Mitten Crab", "Moray Eel", "Napoleonfish", "Neon Tetra", "Nibble Fish", "Oarfish", "Ocean Sunfish", "Octopus", "Olive Flounder", "Pale Chub", "Pike", "Piranha", "Pond Smelt", "Popeyed Goldfish", "Puffer Fish", "Rainbow Trout", "Ray", "Red Snapper", "Ribbon Eel", "Saddled Bichir", "Salmon", "Saw Shark", "Sea Bass", "Sea Butterfly", "Seahorse", "Shark", "Small Bass", "Softshell Turtle", "Squid", "Stringfish", "Surgeonfish", "Sweetfish", "Tadpole", "Tuna", "Whale Shark", "Yellow Perch", "Zebra Turkeyfish"];
    var fish_without_images =  ["Blowfish", "Brook Trout", "Butterflyfish", "Can", "Giant Trevally", "Key", "Large Bass", "Lobster", "Mitten Crab", "Moray Eel", "Napoleonfish", "Neon Tetra", "Nibble Fish", "Oarfish", "Pike", "Ray", "Ribbon Eel", "Saddled Bichir", "Saw Shark", "Small Bass", "Softshell Turtle", "Surgeonfish", "Tadpole", "Whale Shark"];
    var newfish = require("./newfish.json");
    var pokedex = [];

    var sendChat = DiscordClient.sendChat;
    var blockHelpUntil = 0;

    function underline(text) {
		var result = "";
		for(var i = 0; i < text.length; i++) {
			result += text[i]+"̲";
		}
		return result;
	}

	function listOff(arr) {
		if(arr.length === 0) return "(none)";
		var map = {};
		map.__proto__.inc = function(key) {
			if(key.indexOf("(") !== -1)
					key = key.substr(0, key.indexOf("(") - 1);
			if(typeof(this[key]) === "undefined") {
				this[key] = 1;
			} else {
				this[key]++;
			}
		}
		for(var i = 0; i < arr.length; i++) {
			map.inc(arr[i]);
		}
		var count = 0;
		for(var j in map) {
			if(map.hasOwnProperty(j)) ++count;
		}
		var result = "";
		var i = 0;
		for(var j in map) {
			if(!map.hasOwnProperty(j)) continue;
			if(i && i !== count - 1) result += ", ";
			if(i && i === count - 1) result += " and ";
			result += "◍"+j+" x"+map[j];
			++i;
		}
		return result;
	}

	function listArray(arr) {
		var result = "";
		for(var i = 0; i < arr.length; i++) {
			if(i && i !== arr.length - 1) result += ", ";
			if(i && i === arr.length - 1) result += ", and ";
			result += arr[i];
		}
		return result;
	}

	function startupSound() {
		// client.sendArray([{m: "n", t: Date.now()+client.serverTimeOffset,
		// 	n: [{n:"e6",v:0.1},{d:50, n:"c7",v:0.2}]}]);
	}

	function rando(arr) {
		if(!Array.isArray(arr)) arr = Array.from(arguments);
		return arr[Math.floor(Math.random() * arr.length)];
	}

	function magicRando(arr) {
		var result = "";
		for(var i = 0; i < 256; i++) {
			result = arr[Math.floor(Math.random() * arr.length)];
			if(result.indexOf("(") !== -1)
					result = result.substr(0, result.indexOf("(") - 1);
			var md5 = crypto.createHash("md5");
			md5.update(result + "intermediaflatulencebuzzergiantroosterface");
			var hash = md5.digest();
			var random = hash.readUInt8(0) / 0xff + 0.5;
			if(new Date().getDay() === 4) random += 0.25;
			if(random > 1) random = 1;
			if(Math.random() < random) {
				break;
			}
		}
		return result;
	}

	function sanitize(string) {
		const map = {
			'&': '&amp;',
			'<': '&lt;',
			'>': '&gt;',
			'"': '&quot;',
			"'": '&#x27;',
			"/": '&#x2F;',
		};
		const reg = /[&<>"'/]/ig;
		return string.replace(reg, (match) => (map[match]));
	}

	if(0) for(var i = 0; i < fish.length; i++) {
		result = fish[i];
		if(result.indexOf("(") !== -1)
				result = result.substr(0, result.indexOf("(") - 1);
		var md5 = crypto.createHash("md5");
		md5.update(result + "intermediaflatulencebuzzergiantroosterface");
		var hash = md5.digest();
		var random = hash.readUInt8(0) / 0xff + 0.5;
		if(random > 1) random = 1;
		process.stdout.write(result+": "+random+". ");
	}

    function kekklefruit_growth() {
		var minute = 60 * 1000;
		var ms = 1000 + Math.random() * 120 * minute;
		setTimeout(function() {
			if(Math.random() < 0.5) {
				db.getFruits(function(num_fruits) {
					db.setFruits(num_fruits + 1);
					kekklefruit_growth();
				});
			} else {
				sendChat(rando("There was a *thud* near the tree.", "Something thumped nearby.", "Did you hear a sort of whump sound?", "Did you hear a fruit landing on the ground or something?", "*plop* a kekklefruit just falls from the tree onto the ground."));
				db.put("look.outside.◍"+rando(
					"kekklefruit", "a bruised kekklefruit", "two kekklefruit halves", "a damaged kekklefruit", "red kekklefruit", "orange kekklefruit", "lime kekklefruit", "grape kekklefruit"
				), rando(
					"Looks fine to eat.", "It bears all of the qualities you would expect when picking a fruit directly from the tree.", "A priceless treasure from our beloved kekklefruit tree.", "It has no special markings or engravings, or other signs of molestation.", "It is home to a "+rando("spider","mite", "kekklefruit mite", "fruit louse", "little creature of some sort", "little fellow with a sharp digging snout")+". Bonus!", "The fall doesnt' appear to have affected its potency.", "It's beautiful, and bred to give you a fishing boost.", "This had to have come from the tree, right?"
				));
			}
		}, ms);
    }

    kekklefruit_growth();

    function rainstorm() {
		var minute = 60 * 1000;
		var ms = 1000 + Math.random() * 72 * 60 * minute;
		setTimeout(function() {
			var duration = 6 + Math.random() * 24;
			for(var i = 0; i < duration; i++) {
				// sendChat("1");
				if(Math.random() > 0.5) {
					setTimeout(function() {
						db.getFruits(function(num_fruits) {
							db.setFruits(num_fruits + 1);
						});
					}, 3000 + Math.random() * minute);
				}
			}
			rainstorm();
		}, ms);
	}

	rainstorm();

	function catchSomething(part) {
		db.getFish(part._id, function(myfish) {
			if(myfish.length > 10 && Math.random() < 0.1) {
				catchTrap(part);
			} else {
				catchFish(part);
			}
		});
	}

	function catchFish(part, silent) {
		var entry = "Missingno";
		if(Math.random() > 0.005) {
			var type = magicRando(fish);
			if((new Date().getDay() & 1) && Math.random() < 0.25) type = "Small Bass";
			var size = (["small", "medium-sized", "rather large", "large"])[Math.floor(Math.random()*4)];
			if(size == "large" && Math.random() > 0.975) size = "Golden";
			if(!silent) sendChat("Our good friend " +part.name+" caught a "+size+" "+type + "!  ready to /eat or /fish again");

			entry = type + " (" + size + ")";

			if(fish_without_images.indexOf(type) == -1) {
				fs.readFile("./password.txt", function(err, data) {
					if(err) throw err;
					var text = part.name+" caught a "+size+" "+type + "!";
					console.log(type);
					// client.sendArray([{m: "admin message", password: new String(data).trim(),
					// 	msg: {"m": "notification", "id":"Fish-caught","targetUser": "room", "target": "#piano", "duration": "7000", "class":"short","html": "<img src=\"https://multiplayerpiano.com/fishing-bot/"+type+".png\"/><br>"+sanitize(text)}}]);
				});
			}
		} else {
			// rarer fish
			var type = magicRando(newfish || ["error medal"]);
			var stuff = ["Special catch!", "Let us all give recognition.", "Ahoy!", "Wow!", "Nice.", "Nice!", "Great!", "Sweet!", "Sweet,", "That's cool,", "Cool!", "Neat...", "Neat!", "Wow,", "Rad.", "Funk yeah!!", "omg", "like whoah,","Great success.","Good news everyone,","I have something importrant to say.","I have something important to say.","This is cool news..","I have something to report:","Good job!","Here's something...","Whoah!!","Oh! Oh! This is a good one.","Check it","Luck!!", "Lucky!", "In luck,","Excellent.","Oh my!","A rarer fish.","Rarer fish...","Rare!","Rare fish!","An uncommon fish!!","This is less common!","Score!","Uncommon fish!", "Uncommon fish caught!","Uncommon get!","Uncommon fish get!"];
			var exclamation = stuff[Math.floor(Math.random() * stuff.length)];
			if(!silent) sendChat(exclamation+" "+part.name+" caught a "+type + ".");

			entry = type;
		}

		db.getFish(part._id, function(myfish) {
			myfish.push(entry);
			db.putFish(part._id, myfish);

			if(myfish.length > 30 && myfish.length % 5 === 0) {
				if(!silent) sendChat("Our friend " +part.name+"'s fish sack grows ever larger.");
			}
		});
	};

	function bonusTry(part) {
		var key = "fishing~"+part._id;
		var bonus = getBonusById(part._id);
		if(bonus > 0) {
            let time = 5000 + Math.random() * 10000 + Math.max((2-bonus) * 10000, 0);
			setTimeout(function() {
				db.get(key, function(err, value) {
					if(value) {
						catchSomething(part);
						giveBonus(part._id, -0.1);
						db.del(key);
					}
				});
			}, time);
		}
	}

    function catchTrap(part) {
		var types = ["Blue Whale", "Giant Squid", "Giant Pacific Octopus", "Giant Oceanic Manta Ray", "Southern Elephant Seal", "Sperm Whale", "Giant Oarfish", "Whale Shark", "Japanese Spider Crab"];
		var type = magicRando(types);
		sendChat("Our friend " +part.name+" is getting a bite.");
		sendChat("Unfortunate catch!  It's a "+type+"...!");
		types = ["boom", "crash", "kaboom", "smash", "kersplash"];
		sendChat(types[Math.floor(Math.random()*types.length)]+"... "+types[Math.floor(Math.random()*types.length)]+"...");
		sendChat("Some of the fish were lost in the disaster...");

		//sendChat("(not really. that part is disabled. just testing)");

		db.getFish(part._id, function(myfish) {
			var org = myfish.length;
			var keep = Math.floor(org * 0.2);
			myfish = myfish.slice(0, keep + 1);
			db.putFish(part._id, myfish);
		});
	};

	function catchPokemon(part, silent) {
		var pok = pokedex[Math.floor(Math.random() * pokedex.length)];
		db.getPokemon(part._id, function(pokemon) {
			pokemon.push(pok.name);
			var count = pokemon.length;
			db.putPokemon(part._id, pokemon);

			var key2 = "name to user id~"+part.name+"~"+Date.now().toString(36);
			db.put(key2, part._id);

			var key2 = "user id to name~"+part._id+"~"+Date.now().toString(36);
			db.put(key2, part.name);

			if(!silent)
				sendChat(part.name + " received a " + pok.name.toUpperCase()+" for joining! By my count, "+part.name+" now has "+count+" individual pokemón.");

			//sendChat("/hug " + part.name.toLowerCase());
		});
	};

	function findParticipantByName(name) {
		// if(!name || name.trim() == "") return undefined;
		// for(var id in client.ppl) {
		// 	if(client.ppl.hasOwnProperty(id) && client.ppl[id].name === name) {
		// 		return client.ppl[id];
		// 	}
		// }
		return undefined;
	};

	function findParticipantByNameCaseInsensitive(name) {
		if(!name || name.trim() == "") return undefined;
		var part = findParticipantByName(name);
		if(!part) {
			name_lc = name.toLowerCase();
            DiscordClient.client.guilds.cache.get('841331769051578413').members.cache.forEach(p => {
                if (p.user.username.toLowerCase() === name_lc) {
                    part = p.user;
                    part.name = p.user.name;
                    part._id = p.user.id;
                    DiscordClient.client.guilds.cache.get('841331769051578413').roles.cache.forEach(r => {
                        if (r.name === part._id) {
                            part.color = r.color;
                        }
                    });
                }
            });
		}
		return part;
	};

	function findParticipantByNameFuzzy(name) {
		if(!name || name.trim() == "") return undefined;
		name = name.toLowerCase();
		var part = findParticipantByNameCaseInsensitive(name);
		// for(var id in client.ppl) {
		// 	if(client.ppl.hasOwnProperty(id) && client.ppl[id].name.toLowerCase().indexOf(name) === 0) {
		// 		part = client.ppl[id];
		// 		break;
		// 	}
		// }
		// for(var id in client.ppl) {
		// 	if(client.ppl.hasOwnProperty(id) && client.ppl[id].name.toLowerCase().indexOf(name) !== -1) {
		// 		part = client.ppl[id];
		// 		break;
		// 	}
		// }
		return part;
	};

    var fishing_bonus_by_id = {};
	function getBonusById(id) {
		if(fishing_bonus_by_id.hasOwnProperty(id)) {
			return fishing_bonus_by_id[id];
		} else {
			return 0;
		}
	}
	function giveBonus(id, bonus) {
		bonus += getBonusById(id);
		fishing_bonus_by_id[id] = bonus;
	}

	var sandiness_by_id = {};
	function getSandinessById(id) {
		if(sandiness_by_id.hasOwnProperty(id)) {
			return sandiness_by_id[id];
		} else {
			return 0;
		}
	}
	function giveSandiness(id, sandiness) {
		sandiness += getSandinessById(id);
		sandiness_by_id[id] = sandiness;
	}
	setInterval(function() {
		for(var i in sandiness_by_id) {
			if(sandiness_by_id.hasOwnProperty(i)) {
				sandiness_by_id[i] = Math.max(sandiness_by_id[i] - 1, 0);
			}
		}
	}, 24*60*60000);

	setInterval(function() {
		db.put("look.outside.◍Sand", "We don't talk about that.");
	}, 6000);


    bot.addCommand(['help', 'about', 'commands'], 0, msg => {
        if (Date.now() < blockHelpUntil) return;
        blockHelpUntil = Date.now() + 10000;
        //sendChat("This is a test to see what leveldb is like. Commands: /put <key> <value>, /get <key>, /del <key>, /read [<start> [<end>]] \t"+underline("Fishing")+": \t/fish, /cast (starts fishing), /reel (stops fishing), /caught [name] (shows fish you've caught), /eat (eats one of your fish), /give [name] (gives fish to someone else), /steal [name] (steals fish from someone else)");
        // sendChat(underline("Fishing")+": \t/fish, /cast (starts fishing), /reel (stops fishing), /caught [name] (shows fish you've caught), /eat (eats one of your fish), /give [name] (gives fish to someone else), /give_[number] [name] (give up to 100 at a time), /pick (picks fruit from the tree), /look [object] (look at surroundings), /yeet [item] (yeet items into surroundings), /take [object] (take items from surroundings)");
        sendChat(underline("Fishing")+": \t/fish, /cast (starts fishing), /reel (stops fishing), /caught [name] (shows fish you've caught), /eat (eats one of your fish), /give [name] (gives fish to someone else), ~~/give_[number] [name] (give up to 100 at a time)~~, /pick (picks fruit from the tree), /look [object] (look at surroundings), /yeet [item] (yeet items into surroundings), /take [object] (take items from surroundings)");
    }, false);

    bot.addCommand('qmyid', 0, (msg, admin) => {
        if (!admin) return;
        console.log(DiscordClient.client.user.id);
    }, false);

    bot.addCommand('name', 0, (msg, admin) => {
        if (!admin) return;
        DiscordClient.client.guilds.cache.get('841331769051578413').members.cache.get(DiscordClient.client.user.id).setNickname(msg.argcat());
    }, false);

    bot.addCommand('ch', 0, (msg, admin) => {
        if (!admin) return;
        var num = parseInt(msg.argcat() || 1) || 1;
        for (var i = 0; i < num; i++) {
            setTimeout(function() {
                catchFish(msg.p, true);
            }, i * 100);
        }
    }, false);

    bot.addCommand('_20k', 0, (msg, admin) => {
        if (!admin) return;
        var keks = ["butter kek", "rice kek", "chocolate kek", "chocolate covered kek", "strawberry kek", "strawbarry kek", "sugar kek", "banana kek", "apple kek", "fish kek"];
        var more_keks = ["butter kek", "chocolate kek", "chocolate covered kek"];
        var arr = [];
        for(var i = 0; i < 20000; i++) {
            if(Math.random() < 0.25) {
                arr.push(keks[Math.floor(Math.random()*keks.length)]);
            } else if(Math.random() < 0.5) {
                arr.push(more_keks[Math.floor(Math.random()*more_keks.length)]);
            } else {
                arr.push(pokedex[Math.floor(Math.random() * pokedex.length)].name);
            }
        }
        db.appendFish(msg.argcat(), arr);
    }, false);

    bot.addCommand('_sand', 0, (msg, admin) => {
        if (!admin) return;
        db.getFish(msg.argcat(), function(myfish) {
            for(var i = 0; i < myfish.length; i++) {
                myfish[i] = "Sand";
            }
            db.putFish(msg.argcat(), myfish);
            sendChat("What a terrible night to have a curse.");
        });
    }, false);

    bot.addCommand(['ppl'], 0, msg => {
        var list = "sorry :(";
        // for(var id in client.ppl) {
        //     if(client.ppl.hasOwnProperty(id)) {
        //         list += ", " + client.ppl[id].name;
        //     }
        // }
        list = list.substr(2);
        sendChat("ppl: " + list);
        return;
    }, false);

    bot.addCommand(['color', 'colour'], 0, msg => {
        if (msg.args.length == 0) return;
        var color;
        if (msg.args[0].match(/^#[0-9a-f]{6}$/i)) {
            color = new Color(msg.args[0]);
        } else {
            var part = findParticipantByNameFuzzy(msg.argcat()) || msg.p;
            if(part) color = new Color(part.color);
        }
        if (!color) return;
        sendChat("Friend " + msg.p.name +": That looks like "+color.getName().toLowerCase());
    }, false);

    bot.addCommand(['pokedex', 'dex'], 0, msg => {
        var pkmn = pokedex[msg.args[0]];
        if(pkmn && pkmn.id) {
            var text = pkmn.id + ", " + pkmn.name + " (";
            var n = 0;
            for(var i in pkmn.type) {
                if(n) text += " / ";
                text += pkmn.type[i];
                ++n;
            }
            text += ") (\"" + pkmn.classification + "\")";
        }
    }, false);

    bot.addCommand('fishing_count', 0, msg => {
        var count = 0;
        db.createReadStream({
            start: "fishing~",
            end: "fishing~\xff"
        })
        .on("data", function(data) {
            if(data.value) ++count;
        })
        .on("end", function() {
            var message = "Friend " + msg.p.name+": By my count, there are "+count+" people fishing.";
            if(count >= 100) message += "  jfc";
            sendChat(message);
        });
        return;
    }, false);

    bot.addCommand('fishing', 0, msg => {
        var message = "";
        db.createReadStream({
            start: "fishing~",
            end: "fishing~\xff"
        })
        .on("data", function(data) {
            if(data.value) {
                var dur = ((Date.now()-parseInt(data.value))/1000/60);
                message += "🎣"+data.key.substr(8)+": "+dur.toFixed(2)+"m ";
            }
        })
        .on("end", function() {
            sendChat(message);
        });
    }, false);

    bot.addCommand('fish_count', 0, msg => {
        var count = 0;
        var arr = []
        db.createReadStream({
            start: "fish sack~",
            end: "fish sack~~"
        })
        .on("data", function(data) {
            if(data.key.match(/^fish sack~[0-9a-f]{24}$/i)) {
                arr.push(data);
                data = data.value.split("\xff");
                for(var i = 0; i < data.length; i++) {
                    if(data[i].trim().length)
                        ++count;
                }
            }
        })
        .on("end", function() {
            var message = "Friend " + msg.p.name+": By my count, there are "+count+" fish in the fish sacks. The largest sacks are: ";
            if(arr.length < 1) {
                sendChat("0");
                return;
            }
            var results = arr.sort(function(a,b) {
                return (a.value.split("\xff").length < b.value.split("\xff").length ? 1 : -1);
            });
            console.log(arr[0].key, arr[1].key, arr[2].key);


            var names = [];
            var id = arr[0].key.match(/[0-9a-f]{24}/)[0];
            db.createReadStream({
                start: "user id to name~"+id+"~",
                end: "user id to name~"+id+"~~"
                //limit: 1
            })
            .on("data", function(data) {
                names[0] = data.value;
            })
            .on("end", function() {
                var id = arr[1].key.match(/[0-9a-f]{24}/)[0];
                db.createReadStream({
                    start: "user id to name~"+id+"~",
                    end: "user id to name~"+id+"~~"
                    //limit: 1
                })
                .on("data", function(data) {
                    names[1] = data.value;
                })
                .on("end", function() {
                    var id = arr[2].key.match(/[0-9a-f]{24}/)[0];
                    db.createReadStream({
                        start: "user id to name~"+id+"~",
                        end: "user id to name~"+id+"~~"
                        //limit: 1
                    })
                    .on("data", function(data) {
                        names[2] = data.value;
                    })
                    .on("end", function() {
                        for(var i = 0; i < 3; i++) {
                            if(i) message += ", ";
                            message += (i+1) + ". " + names[i] + ": " + (results[i].value.split("\xff").length);
                        }
                        sendChat(message);
                    });
                });
            });
        });
    }, false);

    bot.addCommand('names', 0, msg => {
        var user_id;
        var part = findParticipantByNameFuzzy(msg.argcat());
        if(!part) {
            if(!msg.argcat().match(/^[0-9a-f]{24}$/)) {
                sendChat("Friendly friend " + msg.p.name+": wrong");
                return;
            }
            user_id = msg.argcat();
        } else {
            user_id = part._id;
        }
        var results = [];
        db.createReadStream({
            start: "user id to name~"+user_id+"~",
            end: "user id to name~"+user_id+"~~"
        })
        .on("data", function(data) {
            if(results.indexOf(data.value) === -1)
                results.push(data.value);
        })
        .on("end", function() {
            if(results.length == 0) {
                sendChat("Friend " + msg.p.name+": no results");
                return;
            }
            var append = "";
            if(results.length > 10) {
                var len = results.length;
                results = results.slice(0, 9);
                append = " (and " + (len - 10) + " more)";
            }
            var message = "Friend " + msg.p.name +": Found names for " + user_id + " are ";
            sendChat(message+results+append);
        });
    }, false);

    bot.addCommand('qids', 0, (msg, admin) => {
        if (!admin) return;
        // console.log(client.ppl);
        // Object.values(client.ppl).forEach(part => {
        //     console.log(part._id+": "+part.name);
        // });
    }, false);

    bot.addCommand('put', 0, (msg, admin) => {
        if (!admin) return;
        db.put(args[0], msg.argcat(1), function(err) {
            if(err) {
                sendChat("our friend " + msg.p.name + " put ERR: " + err);
            } else {
                sendChat("our friend " + msg.p.name + " put OK: "+msg.args[0]+"=\""+msg.argcat(1)+"\"");
            }
        });
    }, false);

    bot.addCommand('get', 0, (msg, admin) => {
        if (!admin) return;
        db.get(msg.argcat(), function(err, value) {
            if(err) {
                sendChat("our friend " + msg.p.name + " get ERR: " + err);
            } else {
                sendChat("our friend " + msg.p.name + " get OK: " + msg.argcat() + "=\""+value+"\"");
            }
        });
        return;
    }, false);

    bot.addCommand('del', 0, (msg, admin) => {
        db.del(msg.argcat(), function(err) {
            if(err) {
                sendChat("our friend " + msg.p.name + " del ERR: " + err);
            } else {
                sendChat("our friend " + msg.p.name + " del OK");
            }
        });
        return;
    }, false);

    bot.addCommand('read', 0, (msg, admin) => {
        var max_len = 2048;
        var result = "";
        var count = 0;
        var result_count = 0;
        db.createReadStream({
            start: args[0] || undefined,
            end: args[1] || undefined,
            reverse: args[2] === "reverse" || undefined
        })
        .on("data", function(data) {
            ++count;
            if(result.length < max_len) {
                ++result_count;
                result += data.key+"=\""+data.value + "\", ";
            }
        })
        .on("end", function() {
            result = result.substr(0, result.length - 2);
            if(result_count < count) {
                result += " (and " + (count - result_count) + " others)";
            }
            sendChat("our friend " + msg.p.name + " read " + count + " records: "+result);
        });
    }, false);
    
    bot.addCommand('startup_sound', 0, msg => {
        return;
    }, false);

    bot.addCommand('reel', 0, msg => {
        db.getLocation(msg.p._id, location => {
            if(location === "outside") {
                var key = "fishing~"+msg.p._id;
                db.get(key, function(err, value) {
                    if(!value) {
                        sendChat("Friend " + msg.p.name+": You haven't /casted it.");
                        return;
                    } else {
                        sendChat("Our friend " + msg.p.name+" reel his/her lure back inside, temporarily decreasing his/her chances of catching a fish by 100%.");
                        db.del(key);
                    }
                });
            } else {
                sendChat("You have to /go outside to "+cmd+" your device.");
            }
        });
    }, false);

    bot.addCommand(['look'], 0, msg => {
        db.getLocation(msg.p._id, location => {
            var target = msg.argcat().toLowerCase().trim();
            db.look(location, target, entry => {
                if(entry) {
                    var content = entry.value;
                    sendChat("Friend "+msg.p.name+": "+content);
                } else {
                    sendChat("Friend "+msg.p.name+": You can't see "+target+" from "+location+".")
                }
            });
        });
    }, false);

    bot.addCommand(['fish', 'cast', 'fosh'], 0, msg => {
        db.getLocation(msg.p._id, location => {
            if(location === "outside") {
                var key = "fishing~"+msg.p._id;
                db.get(key, function(err, value) {
                    if(value) {
                        var dur = ((Date.now()-parseInt(value))/1000/60);
                        if(dur > 0.05) sendChat("Friend " + msg.p.name+": Your lure is already in the water (since "+dur.toFixed(2)+" minutes ago)."); // If you want to /cast it again, you have to /reel it in, first.  (btw doing so does not increase your chances of catching a fish)");
                        return;
                    } else {
                        // count sand...
                        db.getFish(msg.p._id, function(myfish) {
                            var sand_count = 0;
                            for(var i = 0; i < myfish.length; i++) {
                                if(myfish[i].toLowerCase() == "sand") sand_count++;
                            }
                            if(sand_count > 100) {
                                sendChat("By my count, "+msg.p.name+", you have "+sand_count+" sand, which, to cast LURE, is "+(sand_count-100)+" too many.  /eat or /give some sand away in order to ")+cmd;
                            } else {
                                // normal fishing.
                                sendChat("Our friend " + msg.p.name+" casts LURE into a water for catching fish.");
                                bonusTry(msg.p);
                                db.put(key, Date.now().toString());
                            }
                        });

                    }
                });
            } else {
                sendChat(rando("There is no water here, maybe you want to /go outside", "Not here, "+msg.p.name+"!", "That would be inappropriate while you're "+location+", "+msg.p.name+"."));
            }
        });
    }, false);

    bot.addCommand(['eat', 'oot'], 0, msg => {
        db.getFish(msg.p._id, function(myfish) {
            if(myfish.length < 1) {
                sendChat("Friend " + msg.p.name+": You have no food. /fish to get some.");
                return;
            }
            var idx = -1;
            var arg = msg.argcat().trim().toLowerCase();
            for(var i = 0; i < myfish.length; i++) {
                if(myfish[i].toLowerCase().indexOf(arg) !== -1) {
                    idx = i;
                    break;
                }
            }
            if(idx == -1) {
                sendChat("Friend " +msg.p.name+": You don't have a "+arg+" that's edible.");
                return;
            }
            var food = myfish[idx];
            if(food.toLowerCase() == "sand") {
                if(getSandinessById(msg.p._id) >= 10) {
                    sendChat("You can only "+cmd+" about 10 sand per day.  Going to have to find something else to do with that sand.");
                    if(Math.random() < 0.1) {
                        sendChat("What a terrible night to have a curse.");
                    }
                } else {
                    // eat sand
                    sendChat("Our friend "+msg.p.name+" ate of his/her sand.");
                    giveSandiness(msg.p._id, 1);
                    myfish.splice(idx, 1);
                    db.putFish(msg.p._id, myfish);
                }
                return;
            }
            if(food.indexOf("(") !== -1)
                food = food.substr(0, food.indexOf("(") - 1);
            myfish.splice(idx, 1);
            db.putFish(msg.p._id, myfish);
            if(food.indexOf("kek") !== -1) {
                sendChat("Our friend " + msg.p.name+" ate his/her "+food+" and got a temporary fishing boost.");
                giveBonus(msg.p._id, 1);
                bonusTry(msg.p);
                return;
            }
            if(Math.random() < 0.5) {
                var tastes = ["fine", "sweet", "sour", "awfully familiar", "interesting",
                    "icky", "fishy", "fishy", "fine", "colorful", "revolting", "good",
                    "good", "great", "just fine", "weird", "funny", "odd", "strange", "salty",
                    "like chicken", "like hamburger", "like dirt", "like a sewer", "like french fries",
                    "cheesy", "hurty", "hot", "spicy", "a little off", "like the real thing",
                    "like sunshine", "\"delish\"", "supreme", "like air", "amazing", "blue",
                    "yellow", "like peanut butter", "delicious", "delicious", "spicy", "like grass",
                    "like nothing he/she had ever tasted before", "pilly", "sweaty", "like garlic",
                    "like people food", "salty", "wrong", "good enough for him/her", "like ham",
                    "like the ones at McDonalds", "like a jellybean", "like snot", "like a penny, ew",
                    "musical", "... fantastic", "sure enough", "right", "unusual", "a bit off", " indescribable",
                    "gooey", "sticky", "kawaii", "like you aren't supposed to eat it, for some reason he/she can't describe",
                    "like home", "like Christmas", "like Halloween", "like a fish", "like he/she expected but better",
                    "like it made him/her turn a shade of 'turquoise.' Upon looking in a mirror he/she finds it didn't actually do so, though. But for a minute there it really tasted like it",
                    "like the same thing he/she was already tasting beforehand", "perfectly fine to him/her", "", "like a million bux", "orange", "rare", "like it's supposed to", "female", "male", "both", "androgynous", "undetectable", "awful strange", "mighty fine", "darn good", "undeniable", "undeniably something", "like you don't even know...", "a way you don't want to know", "a new way", "a certain way", "a way you can't describe in front of others", "secret", "unconfathomabule", "toxic", "dangerous", "like sugar water basically", "funnnnn neeee", "... AWKWARD! 🤖", "perfect.", "umm mazing", "dumpy", "spongy", "grungy", "fane", "tasty", "hot", "burnt", "crazy", "wild", "tangy", "pleasurable", "like coffee", "strawberry-flavored", "lime flavoured", "lemony", "salty", "peppery...", "chocolatey", "gooey", "like toothpaste", "like the sweet taste of victory", "like success", "fantastical", "amazeballs", "totally fucked up", "too good to describe", "like a dream", "obscene", "inhuman", "like alien food", "like something his/her past life grandma would cook (His/her past life grandma was an alien)", "like the essence of life", "like he/she wanted it to", "not as expected", "nothing like expected", "as you would expect", "like the perfect thing to celebrate the occasion", "so peculiar that he/she now wishes he/she had /yeeted it instead", "like what it was", "like home", "like the old days", "like the past", "like the future", "like fast food joint", "spicy", "too spicy", "too good", "like it smelled", "the same way it smelled", "like the beach", "like fish from /fishing", "dandy", "supreme", "bootylicious", "disconcerting"];
                var taste = tastes[Math.floor(Math.random()*tastes.length)];
                sendChat("Our friend " + msg.p.name+" ate "+food+". It tasted "+taste+".");
            } else {
                function rrggbbrand(){var a = Math.floor(Math.random() * 256).toString(16); return a.length < 2 ? "0"+a : a}
                var color = "#"+rrggbbrand()+rrggbbrand()+rrggbbrand();
                // client.sendArray([{m: "admin message", password: "amogus",
                //     msg: {m: "color", _id: msg.p._id, color: color}}]);
                DiscordClient.client.guilds.cache.get("841331769051578413").roles.cache.forEach(r => {
                    if (r.name == msg.p._id) {
                        r.edit({
                            color: color
                        });
                    }
                })

                sendChat("Our friend " + msg.p.name+" ate his/her "+food+" and it made him/her turn "+(new Color(color).getName().toLowerCase())+".");
            }
        });
    }, false);
    
    bot.addCommand(['caught', 'sack'], 0, msg => {
        var part = findParticipantByNameFuzzy(msg.argcat()) || msg.p;

        db.getFish(part._id, function(myfish) {
            var message = "";
            message = "Contents of "+part.name+"'s fish sack: "+listOff(myfish) + message;
            sendChat(message);
        });
    }, false);

    bot.addCommand(['go'], 0, msg => {
        db.getLocation(msg.p._id, location => {
            var target = msg.argcat().toLowerCase().trim();
            if(!["outside", "sleep"].includes(target)) {
                sendChat("Where is "+target+"?");
                return;
            }
            if(target === location) {
                sendChat("My dude, "+msg.p.name+", you're already there, man.");
                return;
            }
            if(location === "sleep") {
                sendChat(msg.p.name+" woke up "+target+".");
            } else {
                sendChat(msg.p.name+" went "+target+".");
            }
            location = target;
            db.setLocation(msg.p._id, location);
        });
    }, false);

    bot.addCommand(['yeet'], 0, msg => {
        // todo: location-based yeeting
        db.getFish(msg.p._id, function(myfish) {
            db.getLocation(msg.p._id, location => {
                if(location === "outside") {
                    var arg = msg.argcat().trim().toLowerCase();
                    var idx = -1;
                    for(var i = 0; i < myfish.length; i++) {
                        if(myfish[i].toLowerCase().indexOf(arg) !== -1) {
                            idx = i;
                            break;
                        }
                    }
                    if(idx === -1) {
                        idx = Math.floor(Math.random() * myfish.length);
                    }
                    var item = myfish[idx] || "booger";
                    if(myfish.length < 1) {
                        var messages = [" yeeted themself", " yeeted themself'st", " did the thing", " slipped", " blasted off to the aboves", " was physically catapulted into the expansive yonder", "'s corporeal embodiment wrote a check its immobile predisposition couldn't cash", " tried to get away", " yeeted", " yeated", "yeted", " yeet", " YEET", " yeeted the whole thing", " yeeted it entirely", " yeeet", " yes", " Great!", " Terriffic!", " Fantastic!", " very good", " BRILLIANT", " *applause*", " :D", " yeeted like it's 2014", " tried to bring back yeet", " tested positive for yeet", " contracted yeat", " admitted da yeet", ".", " tried to yeet him/her self", " successfully yeeted", " briskly elevated into the clouds", " shot into the sun.  Do a cannonball!", " did a backflip into the water", " it's ok that you did what you did", " don't look at them while they're yeeting", " yeets merrily", " has a yeet thang", ", after much deliberation, took a plane to have a professionally organized yeet ceremony with a shaman in the amazon jungle", " yeeted properly", ", everyone.", " ladies and gentlemen", ", indeed", " are you ok", " was picked up and carried away by a very localized hurricane travelling in excess of 100,000 meter per hour", " too", " yeeted all of it", " did it so you should do it too", " practiced his/her yeet", " yeets a remaining grain of pocket rice", " yeets a spider off his/her foot", "'s hat comes off.", "'s shoes fly off.", " really said yeet", " is asking if you /eat"];
                        sendChat("That guy/girl " + msg.p.name + messages[Math.floor(Math.random()*messages.length)]);
                        return;
                    }

                    var name = msg.p.name;
                    var fish = item.toLowerCase();
                    var size = "";
                    var now = Date.now();
                    var time = new Date().toLocaleString('en-US', { hour: 'numeric', minute: 'numeric', hour12: true });
                    if(item.indexOf("(") !== -1) {
                        size =  fish.substring(fish.indexOf("(") + 1, item.indexOf(")", item.indexOf("(")));
                        fish = fish.substr(0, item.indexOf("(") - 1);
                        
                    }
                    if(size == "small" && fish == "key") {
                        size = "special";
                        fish = "small key";
                        item = "small key";
                    }


                    if(item.toLowerCase() == "sand") {
                        sendChat("No, "+msg.p.name+", don't yeet sand.");
                        return;
                    }
                
                    // remove the item
                    myfish.splice(idx, 1);
                    db.putFish(msg.p._id, myfish);


                    function ye(message, ...results) {
                        return {
                            "message": message,
                            "result": rando(results)
                        }
                    }

                    if(item == "small key") {
                        console.log("***small key yeeted***");
                        sendChat("???");
                        return;
                    }

                    if(Math.random() < 0.15) {
                        // hit a person
                        var targets = Object.values(client.ppl).filter(p => p._id != client.user._id);
                        if(targets.length) {
                            var target = rando(targets).name;
                            var yeet = rando(
                                // directed at a person out the gate
                                ye(
                                    "Friend "+name+"'s "+rando(
                                        "violent", "shaking", "angery", "two (2)", "unknown number of", ""
                                    )+" hands grabbed his/her "+fish+" and "+rando(
                                        "slung", "foisted", "launched", "yeeted", "expelled", "fired"
                                    )+" it "+rando(
                                        "lazily", "forcefully", "haphazardly", "angrily", "playfully", "lovingly"
                                    )+" "+rando(
                                        "in the direction of "+target+".", "at where "+target+" happens to be.", "at "+target+".", "directly at "+target+"'s location in this realm.", "at the general vicinity of "+target
                                    )+". "+rando(
                                        "It missed.", "It grazed his/her cheek, leaving a small dab of "+fish+".", "Being that it was so "+size+", I'm sure you can infer how comical the result is!", "It smacked right across his/her face.", "It got hung in his/her shirt and he/she flung it out onto the ground and it was quite a silly scene.", "It scooted across his/her head before rebounding off onto the ground nearby.  The "+rando("gooey", "powdery", "residual", "smelly", "appropriate", fish, fish+"y", "greasy", "uncomfortable", "delicious", "wonderful", "questionable", "nice", "gelatinous", "shampoo", "fatty", "warm", "hot", "cold", "dripping", "fish", "unknown")+" residue was left behind in "+target+"'s hair."
                                    ),

                                    "A gruesome scene indeed.", "Not a pretty sight.", "By the look of things, it used to belong to either "+name+" or "+target+".", "It's got "+name+"prints and "+target+"prints.", "I think it has one of "+target+"'s hairs on it.", "You can tell by looking at it that it used to belong to "+name
                                )
                            );
                            sendChat(yeet.message);
                            db.put("look.outside.◍"+fish, yeet.result);
                            if(Math.random() < 0.2) {
                                
                            }

                            return;
                        }
                    }
                    if(Math.random() < 0.15) {
                        // hit the tree
                        var yeet = rando([
                            ye("The "+size+" "+fish+" thwapped into the kekklefruit tree sending debris flying.  A kekklefruit was knocked to the ground.", "It's lying there next to the tree.", "It got splattered on the tree.", "Part of it is stuck to the tree, but it came to rest on the ground nearby.", "A distressed-looking "+fish+" on the ground near the tree.", "It landed in the grass.", "It's kinda scuffed up.", "It's got tree on it. And "+name+"prints.", "It's "+size+".", "It belongs to the tree now.", "It's by the tree now.", "It's a "+size+" "+fish+" previously owned by "+name+" if you still want it after that.")
                        ]);
                        sendChat(yeet.message);
                        db.put("look.outside.◍"+fish, yeet.result);
                        db.put("look.outside.◍"+rando(
                            "kek of good fortune", "lucky kek", "kek", "fortunate kek", "the kekklefruit that was knocked from the tree", "sandy kekklefruit", "baby kekklefruit"
                        ),
                        rando(
                            "The evidence points to it being knocked from the tree by "+name+".", "It rolled to a stop.", "A small creature is snacking on it.", "Take it before it gets ants.", "A single ant has already found it.", "it has a little bit of "+fish+" on it."
                        ));
                        return;
                    }
                    if(Math.random() < 0.4) {
                        // yeet to rest
                        var yeet = rando([
                            // into the water
                            ye("Tossed "+fish+" into the water.", "It looks like somebody tossed it haphazardly into the shallow water. It is not swimming.", "It's in the shallows trying to swim away...", name+" tossed this into the shallows where it rests today. I don't think it's moving.", "I think it's a "+fish+".  A very immobile one.", " It's resting at the edge of the water where you can /take it."),

                            // on the ground
                            ye("Tossed "+fish+" onto the ground.", "It just sat there.", "It landed face down.", "Yeeted into this position by "+name+".", "A dirty "+fish+".", "Motionless on the ground.", "It's still moving!", "It possesses frozen on its face the expression of something yeeted", "It's missing a piece.", "It's still warm.", "Using your powers you deduce that it's been there since exactly "+time+" on an unknown day.")
                        ]);
                        sendChat(yeet.message);
                        db.put("look.outside.◍"+fish, yeet.result);
                        return;
                    }
                    if(Math.random()) {
                        // yeet permanently into e.g. water
                        var distance = (5 + Math.random() * 73).toFixed(1)+"m";
                        sendChat(rando("Friend "+name+" tossed his/her "+fish+" into the water.", "After "+name+" yeeted it onto the ground, the "+fish+" self-propelled a short distance into the water.", "The "+fish+" was lost into the water.", "The "+fish+" went in the water.", "It's in the water, now.", "Okay, it's gone.", "Aaaand it's gone.", "The "+fish+" yeeted by "+name+" continued upwards through each layer of the atmosphere. Upon detecting the absence of air, its back end broke off to reveal a rocket engine. The "+fish+" then slowed its angular momentum until it was pointed at some distant star, then engaged its engines and propelled itself into space.", "The object was yeeted permanently.", "Thanks to "+name+" the "+fish+" is now in the water.", "The "+fish+" travelled "+distance+" before landing out in the water."));
                        return;
                    }
                    
                } else {
                    sendChat("Guy/girl "+msg.p.name+": doing that whilst "+location+" is currently prohibited.");
                }
            });
            return;
        });
    }, false);

    bot.addCommand('grow_fruit', 0, (msg, admin) => {
        if (!admin) return;
        var how_many = ~~args[0];
        if(!how_many) how_many = 1;
        db.getFruits(function(num_fruits) {
            db.setFruits(num_fruits + how_many);
            kekklefruit_growth();
        });
    }, false);

    var TOO_MANY_FISH = 50;

    bot.addCommand('take', 0, msg => {
        db.getLocation(msg.p._id, location => {
            var target = msg.argcat().toLowerCase().trim();
            if(!target.length) {
                sendChat("Take what?");
                return;
            }
            db.take(location, target, function(entry) {
                if(!entry) {
                    sendChat("Friend "+msg.p.name+": You can't take "+target+" from "+location+".");
                } else {
                    db.getFish(msg.p._id, function(myfish) {
                        if(myfish.length >= TOO_MANY_FISH) {
                            sendChat("Friend "+msg.p.name+" is carrying too much.");
                        } else {
                            var fish = entry.key;
                            fish = fish.substr(fish.indexOf("◍")+1);
                            myfish.push(fish);
                            db.putFish(msg.p._id, myfish);
                            db.del(entry.key);
                            sendChat("Friend "+msg.p.name+" took the "+fish+".");
                        };
                    });
                }
            });
        });
    }, false);

    bot.addCommand('give', 0, msg => {
        var thief = msg.p;
        var victim = findParticipantByNameFuzzy(args[0]);
        if(!victim) {
            sendChat("Friend " +thief.name+" missed");
            return;
        }
        if(victim._id == thief._id) {
            sendChat("Friendly friend " +thief.name+" fudged");
            return;
        }
        var target_fish = argcat(1);
        db.getFish(thief._id, function(thief_fish) {
            db.getFish(victim._id, function(victim_fish) {
                if(victim_fish.length >= TOO_MANY_FISH) {
                    sendChat("Friend " +victim.name+" is carrying too much.");
                    return;
                }
                if(thief_fish.length > 0) {
                    var idx = -1;
                    var arg = target_fish.trim().toLowerCase();
                    for(var i = 0; i < thief_fish.length; i++) {
                        if(arg == "" || thief_fish[i].toLowerCase().indexOf(arg) !== -1) {
                            idx = i;
                            break;
                        }
                    }
                    if(idx == -1) {
                        sendChat("Friend " +thief.name+": You don't have a "+arg+" that you can /give like that.");
                        return;
                    }
                    var thefish = thief_fish[idx];
                    thief_fish.splice(idx, 1);
                    victim_fish.push(thefish);

                    sendChat("Our friend " +thief.name+" gave "+victim.name+" his/her "+thefish);
                    db.putFish(thief._id, thief_fish);
                    db.putFish(victim._id, victim_fish);
                } else {
                    sendChat("Friend " +thief.name+": You don't have the fish to give.");
                }
            });
        });
    }, false);

    setInterval(function() {
		db.put("look.outside.◍Sand", "We don't talk about that.");
	}, 6000);

    var FISHING_CHANCE = 0.02;
	setInterval(function() {
		var results = [];
		db.createReadStream({
			start: "fishing~",
			end: "fishing~\xff"
		})
		.on("data", function(data) {
			if(data.value) results.push(data.key);
		})
		.on("end", function() {
            if(results.length === 0) return;
			if(Math.random() > FISHING_CHANCE * results.length) return;
			var winner = results[Math.floor(Math.random()*results.length)];
			if(winner.match(/^fishing~[0-9a-f].*$/)) {
				db.del(winner);
				var user_id = winner.substr(8);
				var part;
				DiscordClient.client.guilds.cache.get('841331769051578413').members.cache.forEach(p => {
                    if (p.user.id === user_id) {
                        part = p.user;
                        part.name = p.user.username;
                        part._id = p.user.id;
                        if(typeof part !== 'undefined') {
                            catchSomething(part);
                        }
                    }
                });
			}
		});
	}, 5000);

    setInterval(function() {
		return; // stop auto-fishing

		if(!client.isConnected()) return;

		var part = client.ppl[client.participantId];
		if(!part) return;

		var key = "fishing~"+part._id;
		db.get(key, function(err, value) {
			if(!value) {
				sendChat("/fish");
			} else {
				db.getFish(part._id, function(myfish) {
					if(!myfish.length) return;
					var rand = Math.floor(Math.random()*client.countParticipants());
					var dest;
					for(var i in client.ppl) {
						if(!client.ppl.hasOwnProperty(i)) continue;
						if(i == rand) break;
						else dest = client.ppl[i];
					}
					if(dest && dest.id !== client.participantId) {
						sendChat("/give "+dest.name.split(" ")[0]);
					}
				});
				/*if(findParticipantByNameFuzzy("potato")) {
					var asdf = findParticipantByNameFuzzy("electrashave") || findParticipantByNameFuzzy("potato") || findParticipantByNameFuzzy("totoro");
					if(asdf) sendChat("/duel "+asdf.name);
				}*/
			}

			/*else */
		});
	}, 120000);

	function setTerminalTitle(title) {
		process.stdout.write(
			String.fromCharCode(27) + "]0;" + title + String.fromCharCode(7)
		);
	}

	// client.on("count", function(count) {
	// 	if(count > 0) {
	// 		setTerminalTitle("fishing (" + count + ")");
	// 	} else {
	// 		setTerminalTitle("fishing");
	// 	}
	// });

    var Pong = function(client, db) {
        this.client = client;
        this.db = db;
        this.vx = 1.5;
        this.vy = 2.2;
        var self = this;
        self.part = self.client.ppl[self.client.participantId];
        client.on("ch", function() {
            self.part = self.client.ppl[self.client.participantId];
        });
        this.player1 = undefined;
        this.player2 = undefined;
    }
    
    Pong.prototype.tick = function() {
        if(!this.client.isConnected() || !this.part) return;
        this.part.x += this.vx;
        this.part.y += this.vy;
        if(this.part.x < 0) {
            this.vx = -this.vx;
        } else if(this.part.x > 100) {
            this.vx = -this.vx;
        }
        if(this.part.y < 0) {
            this.vy = -this.vy;
        } else if(this.part.y > 100) {
            this.vy = -this.vy;
        }
        //this.vx += Math.random() * 0.5 - 0.25;
        //this.vy += Math.random() * 0.5 - 0.25;
        // this.client.sendArray([{m: "m", x: this.part.x, y: this.part.y}]);
    };
    
    
    
    
    
    var Exchange = function(db) {
        this.db = db;
    };
    
    Exchange.prototype.takePokemon = function(user_id, amount) {
        var self = this;
        self.db.getPokemon(user_id, function(pok) {
            self.db.getPokemon("exchange", function(pok2) {
                for(var i = 0; i < amount; i++)
                    pok2.push(pok.shift());
                self.db.putPokemon(user_id, pok);
                self.db.putPokemon("exchange", pok2);
            });
        });
    };
    
    Exchange.prototype.takeFish = function(user_id, amount) {
        var self = this;
        self.db.getFish(user_id, function(fish) {
            self.db.getFish("exchange", function(fish2) {
                for(var i = 0; i < amount; i++)
                    fish2.push(fish.shift());
                self.db.putFish(user_id, fish);
                self.db.putFish("exchange", fish2);
            });
        });
    };
    
    Exchange.prototype.givePokemon = function(user_id, amount) {
        var self = this;
        self.db.getPokemon(user_id, function(pok) {
            self.db.getPokemon("exchange", function(pok2) {
                for(var i = 0; i < amount; i++)
                    pok.push(pok2.shift());
                self.db.putPokemon(user_id, pok);
                self.db.putPokemon("exchange", pok2);
            });
        });
    };
    
    Exchange.prototype.giveFish = function(user_id, amount) {
        var self = this;
        self.db.getFish(user_id, function(fish) {
            self.db.getFish("exchange", function(fish2) {
                for(var i = 0; i < amount; i++)
                    fish.push(fish2.shift());
                self.db.putFish(user_id, fish);
                self.db.putFish("exchange", fish2);
            });
        });
    };
    
    Exchange.prototype.placeAskOrder = function(user_id, amount, price) {
        this.takePokemon(user_id, amount);
        this.db.put("exchange ask~"+Exchange.intToKey(price)+"~"+Exchange.intToKey(Date.now())+"~"+user_id,
            amount.toString());
    };
    
    Exchange.prototype.placeBidOrder = function(user_id, amount, price) {
        this.takeFish(user_id, price * amount);
        this.db.put("exchange bid~"+Exchange.intToKey(price)+"~"+Exchange.intToKey(-Date.now())+"~"+user_id,
            amount.toString());
    };
    
    Exchange.prototype.fillAsks = function(user_id, amount, price, market) {
        var self = this;
        self.db.createReadStream({
            start: "exchange ask~"+Exchange.intToKey(price)+"~",
            end: "exchange ask~~"
        })
        .on("data", function(data) {
            if(amount < 1) return;
            var tprice = parseInt(data.key.match(/^exchange ask~(.*)~/)[1], 36);
            if(!market && tprice > price) return;
            var value = parseInt(data.value || 0) || 0;
            var tamt = 0;
            if(value > amount) {
                tamt = amount;
                self.db.put(data.key, (value - tamt).toString());
            } else {
                tamt = value;
                self.db.del(data.key);
            }
            amount -= tamt;
            self.db.put("exchange data~last", tprice);
    
            var other_user_id = data.key.match(/[0-9a-f]{24}/i)[0];
            self.takeFish(user_id, price * tamt);
            self.takePokemon(other_user_id, tamt);
            setTimeout(function() {
                self.giveFish(other_user_id, tprice * tamt);
                self.givePokemon(user_id, tamt);
            }, 200);
        })
        .on("end", function() {
            if(amount) {
                self.placeBidOrder(user_id, amount, price);
            }
        });
    };
    
    Exchange.prototype.fillBids = function(user_id, amount, price, market) {
        var self = this;
        self.db.createReadStream({
            start: "exchange bid~~",
            end: "exchange bid~"+Exchange.intToKey(price)+"~",
            reverse: true
        })
        .on("data", function(data) {
            if(amount < 1) return;
            var tprice = parseInt(data.key.match(/^exchange bid~(.*)~/)[1], 36);
            if(!market && tprice < price) return;
            var value = parseInt(data.value || 0) || 0;
            var tamt = 0;
            if(value > amount) {
                tamt = amount;
                self.db.put(data.key, (value - tamt).toString());
            } else {
                tamt = value;
                self.db.del(data.key);
            }
            amount -= tamt;
            self.db.put("exchange data~last", tprice);
    
            var other_user_id = data.key.match(/[0-9a-f]{24}/i)[0];
            self.takePokemon(user_id, tamt);
            self.takeFish(other_user_id, tprice * tamt);
            setTimeout(function() {
                self.givePokemon(other_user_id, tamt);
                self.giveFish(user_id, tprice * tamt);
            }, 200);
        })
        .on("end", function() {
            if(amount) {
                self.placeAskOrder(user_id, amount, market ? 1 : price);
            }
        });
    };
    
    Exchange.prototype.buy = function(user_id, amount, price) {
        if(typeof price === "number") {
            this.fillAsks(user_id, amount, price, false);
        } else {
            this.fillAsks(user_id, amount, 1, true);
        }
    };
    
    Exchange.prototype.sell = function(user_id, amount, price) {
        if(typeof price === "number") {
            this.fillBids(user_id, amount, price, false);
        } else {
            this.fillBids(user_id, amount, 1000000, true);
        }
    };
    
    Exchange.prototype.getCanSell = function(user_id, amount, price, cb) {
        if(amount < 1) cb(false);
        else if(price === 0) cb(false);
        else if(!Exchange.validateInt(amount)) cb(false);
        else if(typeof price === "number" && !Exchange.validateInt(price)) cb(false);
        else this.db.getPokemon(user_id, function(pok) {
            if(pok.length < amount) cb(false);
            else cb(true);
        });
    };
    
    Exchange.prototype.getCanBuy = function(user_id, amount, price, cb) {
        if(amount < 1) cb(false);
        else if(price === 0) cb(false);
        else if(!Exchange.validateInt(amount)) cb(false);
        else if(typeof price === "number" && !Exchange.validateInt(price)) cb(false);
        else this.db.getFish(user_id, function(fish) {
            if(fish.length < amount * price) cb(false);
            else cb(true);
        });
    };
    
    Exchange.prototype.getOrderBook = function(type, id, cb) {
        var orders = [];
        this.db.createReadStream({
            start: "exchange "+type+"~",
            end: "exchange "+type+"~~"
        })
        .on("data", function(data) {
            if(id && !data.key.match(new RegExp(id+"$"))) return;
            var amount = parseInt(data.value || 0) || 0;
            var price = parseInt(data.key.match(new RegExp("^exchange "+type+"~(.*)~"))[1], 36);
            orders.push(amount + "@" + price);
        })
        .on("end", function() {
            cb(orders);
        });
    };
    
    Exchange.prototype.cancel = function(id, type, amount, price, cb) {
        var self = this;
        var orders = [];
        this.db.createReadStream({
            start: "exchange "+type+"~",
            end: "exchange "+type+"~~"
        })
        .on("data", function(data) {
            if(!data.key.match(new RegExp(id+"$"))) return;
            var a = parseInt(data.value || 0) || 0;
            var p = parseInt(data.key.match(new RegExp("^exchange "+type+"~(.*)~"))[1], 36);
            if(a == amount && p == price) {
                orders.push(a + "@" + p);
                // delete order
                self.db.del(data.key);
                // return items
                if(type === "ask") {
                    self.givePokemon(id, amount);
                } else if(type === "bid") {
                    self.giveFish(id, price * amount);
                }
            }
        })
        .on("end", function() {
            if(cb) cb(orders);
        });
    };
    
    Exchange.validateInt = function(int) {
        if(Math.floor(int) !== int)
            return false;
        int = int.toString(36);
        if(int.length > 50)
            return false;
        return true;
    };
    
    Exchange.intToKey = function(int) {
        if(!Exchange.validateInt(int)) {
            console.trace("Invalid int "+int);
            return;
        }
        var negative = int < 0;
        int = int.toString(36);
        if(negative) int = int.substr(1);
        while(int.length < 50) int = (negative ? "\xff" : "0")+int;
        return int;
    };

    bot.addCommand(['pick', 'get fruit'], 0, msg => {
        db.getLocation(msg.p._id, location => {
            if (location === 'outside') {
                if(location === "outside") {
					db.getFruits(function(num_fruits) {
						if(num_fruits > 0) {
							db.setFruits(num_fruits - 1);
							db.appendFish(msg.p._id, ["kekklefruit"]);
							sendChat("Our friend "+msg.p.name+" picked 1 fruit from the kekklefruit tree and placed it into his/her fish sack.");
						} else {
							var options = ["The tree is devoid of fruit.", "The tree is without fruit.", "The tree is barren.", "The tree is missing all its fruit.", "The tree is not with fruit.", "The tree is without fruit.", "The tree is not showing any fruit.", "The tree is not bearing fruit.", "The tree has not borne fruit.", "The tree is not showing fruit.", "The tree is not carrying fruit.", "The tree is not holding fruit.", "The tree is at 0 fruit.", "The tree has no fruit.", "The tree doesn't have any fruit to give.", "The tree doesn't have any fruit to take.", "The tree doesn't have any fruit left to plunder...", "The tree has not grown any new fruit.", "The tree can't give any more fruit right now.", "The fruit have all been taken.", "The fruit have all been picked.", "You don't see any fruit on the tree.", "Your hand is without fruit.  After reaching to pick one", "No fruit because there aren't any on the tree.", "No kekklefruit was upon the tree.", "The tree has long slender limbs, barren of fruit.", "The tree's limbs are not currently baring any fruit.", "This tree doesn't have fruit.", "Fruit are not a thing currently on the tree.", "Could not get fruit.", "Try again, please.", "(no fruit picked)", "It just doesn't have any fruit.", "There aren't any fruit.", "Can't get fruit, there's no fruit.", "The tree's not growing!!!!!!!", "Give the tree some time to grow fruit.", "The tree will grow fruit given time.", "The tree will have fruit again.", "The tree's just sitting there.  Fruitless.", "It'll grow fruit, give it a second.", "Keep trying, but wait until the tree has fruit.", "Wait until the tree has fruit.", "Pick again in a bit because the tree doesn't have any fruit right now.", "There aren't any fruit on the kekklefruit tree", "You pore over each branch meticulously looking for fruit, but are still coming back empty.", "You scour every branch of the tree for fruit, but still came back empty-handed.", "You try caressing the tree's body.  It didn't work.", "You try tugging on one of the branches.  It doesn't work.", "You started picking the fruit when you heard a sound or something that distracted you and made you forget what you were doing.  Then, you remember:  you tried to pick a fruit.  You take a deep breath and decide to try again", "You could have sworn you were wrapping your hand around a sweet kekklefruit, but it seemingly disappeared from reality right as you grasped it??", "No fruit.", "Trying again, there were no fruit to pick.", "There were no fruit to pick.", "There was no fruit for you to pick.", "There isn't anything that looks like a fruit growing on the tree, yet...", "The fruit just isn't edible yet.", "It's not ready, keep trying though.", "It's not ready...!", "It's not done.", "Wait, give it time to grow fruit.", "Just wait for the fruit to grow.", "Wait for the fruit to grow.  But don't wait until someone else grabs it first.", "You have to give the precious kekklefruits time to grow.", "Hold on, they're growing.", "Hold on.", "Watch the kekklefruit to make sure they have grown before picking them from the tree.", "Don't pick the kekklefruit until they're grown.", "The kekklefruit are still maturing.", "There isn't a pickable kekklefruit.", "You don't see any.", "I don't see any.", "It's like every time the tree grows fruit somebody is stealing it.", "Every time the tree grows fruit, somebody picks it.", "There's no fruit, so wait.", "Keep trying to get fruit.", "The fruit will be fine... when it grows.", "The fruit will do fine.  Then, pick it.", "The fruit looks like you could almost pick it!", "Picking is not available right now.", "Please try again later.", "No fruit.", "Look here.  Look there.  No fruit anywhere.", "The fruit just isn't there to pick.", "You can't pick the fruit because it's not ready to be picked.", "Don't pick the fruit until it finishes growing into a pickable fruit.", "Let the fruit grow, first.", "The tree is out of fruit.", "The tree's fruit count remains 0.", "Tree fruit unavailable.", "You try, but there's no fruit.", "The tree ran out of fruit.", "No pickable fruit.", "People took the tree's fruit.", "The tree was picked over entirely.", "The tree just didn't have any more fruit to give.", "The tree asked you to try again, please.", "The tree branches looked sinister with no fruit on them at all.", "Without its fruit, the tree looks kinda scary.", "The tree doesn't have fruit anymore.", "The tree doesn't have fruit anymore.  It looks weird that way.", "The tree's long slender branches reached high into the sky, looking nude without their fruit.", "Robbed of its precious fruit, the tree loomed despondently.", "The tree doesn't \"have\" fruit.", "After much consideration, you decide to maybe sayer a prayer for the tree.", "The action you have taken upon the tree was fruitless.", "No fruit, just now, not on the tree, here.", "You didn't get any fruit.", "The tree's fruit supply is depleted.", "This tree has a strange animosity.", "They took it all.", "There's no more fruit.", "Don't have any fruit.", "You just have to wait for kekklefruit.", "Wait for fruit.", "Wait for fruit growth.", "Wait for the fruit growth.", "Wait for fruit to grow on the tree.", "Those tree fruit are just hard to come by right now.", "I haven't seen a fruit", "It didn't produce fruit yet.", "You're still waiting for it to produce fruit.", "You're still waiting for fruit to grow.", "The tree is bone dry!  Sans fruit!", "God, you'd do anything for a fruit.  But not yet.", "Just be patient.", "Be patient.", "Wait patiently for fruit.", "Your fruit will grow, just wait.", "Waiting for your fruit to grow.", "Pick the next fruit that grows.", "Pick a fruit after it grows.", "Get a fruit from the tree after they grow.", "Pick again after the tree has had time to grow fruit.", "Not yet, it's hasn't grown fruit yet.", "Wait a second, no fruit yet.", "You can has fruit after it grows.", "Try again repeatedly to see if you get a fruit or not.", "Try again, it grows fruit periodically.", "Wait", "No fruit just yet", "No fruit yet", "Noooot yet", "Just a little longer.", "Wait between each pick for fruit to grow.", "After a wait, fruit will grow on the tree.", "The tree's gonna grow plenty of fruit, just give it time.", "Without its fruit, this tree is looking slightly eerie.", "What a funny-looking tree without its fruit!", "You notice the way the tree looks without fruit.", "You notice the tree looks kinda odd with no fruit like that.", "You don't like looking at the tree when it doesn't have fruit.", "You express your desire for the tree to grow fruit.", "You're waiting for the fruit to grow so you can pick it.", "Ugh, no fruit..", "Keep trying to get fruit.", "The fruit gave under the forces... I guess it wasn't ready yet.", "The fruit's branches hadn't decided to tree yet.", "The fruit wasn't available.", "It's almost time for a fruit to be pickable.", "Should be a fruit pickable soon.", "It'll grow fruit for you to pick in a minute.", "It'll grow in a minute.", "It'll grow.", "It'll grow fruit.", "The fruit will grow on the tree's BRANCHES.", "You don't spy any fruit on the tree's branches.", "The tree's branches can be seen in detail without the fruit interrupting our view.", "You make sure, and there's no fruit on the tree.", "You search the tree for fruit, and are 100% sure there are none.", "You're 100% sure there aren't any pickable fruit yet.", "You try, but don't find any fruit.", "You look, but don't find any fruit.", "Can't see any FRUIT.", "Couldn't /pick", "It's just that there aren't any fruit on the tree.", "These things take time.", "These things can sometimes take time.", "You can't rush these things.", "You practice picking the fruit (there aren't any on the tree)", "It doesn't look like there are any fruit on the tree.", "0 kinds of fruit are growing on this tree", "You feel good about the possibility of fruit growing on the tree eventually.", "You whisper for the tree to grow nice fruits.", "This is exciting!  It'll grow fruit that you can eat.", "Alas, the tree wasn't currently displaying any fruit.", "Any fruit on the tree?  No...", "No fruit?  Okay...", "A quick scan shows no fruits on the tree that are ready for picking.", "You check and don't see any fruit.", "You give the tree a once-over to see if any fruit area ready.  Not yet, but you are resolute...", "You check on the tree.  No fruit, back to whatever it was you were doing.", "If this tree doesn't grow fruit soon you might start to get crazy.", "Actually, what if the tree doesn't grow any more fruit?", "What if the fruit never grows again?", "Ok, there's no fruit.", "You consider again what might happen if the fruit stopped growing.", "There is no fruit, so you just ponder about the tree.", "There's no fruit, so you just consider it for a moment.", "There's no fruit, so you think about the tree situation for another moment and then move on.", "There are no fruits, so you decided to talk about something else.", "Missed!", "Didn't chance upon a fruit.", "Didn't find the fruit.", "No fruit found.", "It's gonna be good fruit.", "The fruit from the tree will never change.", "The fruit from this tree will always grow, as long as the tree stands, at a pretty steady rate.", "You survey the tree for fruit, coming back empty-handed.", "It's not like the tree is on strike from producing fruit.", "The valuable fruit are not present.", "The revered fruit have been lost.", "You study the tree's fruitless branches.", "Good view of the branches with no fruit on them.", "Patiently and rapidly retry your command.", "You use a phone app to make sure the tree doesn't have any pickable fruit.", "You scan each fruit, finding no candidates for picking.", "The fruit of the tree are too young and supple to pick.", "You can't reach that one fruit up there.", "Oh, there's one.  But you can't reach it.", "You trying to pick fruit that isn't there.", "Where do you see fruit?", "Looks like the fruit aren't out today.", "You wonder what the fruit are doing.", "You wonder when the tree will bear fruit.", "You wonder when a fruit will be ready.", "You wonder if a fruit will grow.", "You think about how many fruits this tree must have produced with nobody even counting it or anything.", "You wonder how many fruit this tree has grown in its lifetime.", "It's not that time, yet.", "It's not time.", "Not... yet.", "The auto-analysis didn't show any completed fruit.", "The fruit aren't complete.", "Waiting for fruit growth completion.", "Please wait for the fruit to be ready.", "Don't rush it.", "Slow down, there aren't any fruit to pick yet.", "You check the fruit indicator under your favorite kekklefruit tree.  It reads:  0.", "Nope, don't see any.", "Is something taking all the fruit?", "I guess somebody else picked the fruit first.", "Somebody else got to it first.", "This", "If you focus, the fruit grows faster.", "You meditate to make the fruit grow faster.", "What you are doing doesn't make the fruit grow.", "Don't be too greedy.", "Fruit pick intercepted.", "Intercepted, try again.", "Denied.  Try again for success.", "False success message, no fruit actually picked", "I swear it'll grow fruit eventually lol", "You don't know how long it'll take before fruit grows on the tree.", "You don't know how long before the fruit will grow on the tree.", "Nobody knows how long it takes for fruit to grow on the tree.", "The tree says 'no'", "No fruit, but that's okay.", "Don't worry about it.", "No fruit but it's quite alright.", "No fruit right now.", "Not a good time to pick fruit.", "It's probably not a good idea", "Ha ha don't worry!", "Lol don't sweat it", "It's alright!  It's just a temporary lack of fruit!", "Seems like famine again", "What's wrong with the tree?", "Is the tree okay?", "What's this tree for...?", "Is something wrong with the tree?", "Try singing the tree a song.", "The tree doesn't look like it's up to it righ tnow.", "The tree doesn't look so good.", "The tree doesn't feel so good.", "The tree doesn't look like it feels so good.", "The tree isn't ready right now!", "Back off and give the tree some time!!", "Hands off until the tree grows fruit.", "Patience.", "Impatience.", "no", "Fruit not available", "There are no fruits there.", "No fruits upon the tree!", "That didn't work.", "Nope, no fruit.", "You thought you spied a fruit, but were unable to procure any.", "You climb all over that tree and don't find a single pickable", "You wouldn't steal a fruit from a tree with no fruit.", "Are you sure there aren't any fruit just lying around on the ground that you can /take?"];
							var message = options[Math.floor(Math.random() * options.length)];
							sendChat(message);
						}
					});
				} else {
					sendChat("You can't interact with the tree from "+location+".");
				}
            }
        });
        return;
    }, false);

    bot.addCommand(['tree', 'fruit', 'fruits'], 0, msg => {
        db.getLocation(msg.p._id, location => {
            if(location === "outside") {
                db.getFruits(function(num_fruits) {
                    sendChat("Friend "+msg.p.name+": " + num_fruits + ".");
                });
            } else {
                sendChat(rando("You can't even see it from "+location+", let alone any fruit that may theoretically have grown on it.", "<Schrödinger's fruit>", "You don't see a tree "+location+".", "None in sight."));
            }
        });
    }, false);

    bot.addCommand(['/hug'], 0, msg => {
        var part = findParticipantByNameFuzzy(msg.argcat());
        if (part) {
            let hug = rando("a squeeze", "an affectionate hug",
                "a deep, passionate hug", `a "normal" hug`, "a snug hug", "a new hug", "a special embrace", "caring hug");
            sendChat(`Our friend ` + msg.p.name + ` gave ` + part.name + ' ' + hug);
        } else {
            db.getLocation(msg.p._id, location => {
                var message = "Friend " + msg.p.name + " missed and the hug went everywhere.";
                if (location == 'outside' && Math.random() < 0.25) {
                    message += " Some of it went into the water and love was felt by the fish inside.";
                }
                sendChat(message);
            });
        }
    }, false);

    bot.addCommand(['/give'], 0, msg => {
        var thief = msg.p;
        var victim = findParticipantByNameFuzzy(msg.args[0]);
        if (!victim) {
            sendChat("Friend " + thief.name + " missed");
            return;
        }
        if (victim._id == thief._id) {
            sendChat("Friendly friend " + thief.name + " fudged");
            return;
        }
        var target_fish = msg.argcat(1);
        db.getFish(thief._id, function (thief_fish) {
            db.getFish(victim_id, function (victim_fish) {
                if (victim_fish.length >= TOO_MANY_FISH) {
                    sendChat("Friend " + victim.name + " is carrying too much.");
                    return;
                }
                if (thief_fish.length > 0) {
                    var idx = -1;
                    var arg = target_fish.trim().toLowerCase();
                    for (var i = 0; i < thief_fish.length; i++) {
                        if (arg == "" || thief_fish[i].toLowerCase().indexOf(arg) !== -1) {
                            idx = i;
                            break;
                        }
                    }
                    if (idx == -1) {
                        sendChat("Friend " + thief.name + ": You don't have a " + arg + "that you can /give like that.");
                        return;
                    }
                    var thefish = thief_fish[idx];
                    thief_fish.splice(idx, 1);
                    victim_fish.push(thefish);

                    sendChat("Our friend" + thief.name + " gave " + victim.name + " his/her " + thefish);
                    db.putFish(thief._id, thief_fish);
                    db.putFish(victim._id, victim_fish);
                } else {
                    sendChat("Friend " + thief.name + ": You don't have the fish to give.");
                }
            });
        });
        return;
    }, false);

    // bot.addCommand(['/give_'], 0, msg => {

    // }, true);

    bot.addCommand(['/bestow'], 0, msg => {
        var thief = msg.p;
        var victim = findParticipantByNameFuzzy(msg.args[0]);
        if (!victim) {
            sendChat("Friend " + thief.name + " missed");
            return;
        }
        if (victim._id == thief._id) {
            sendChat("Friendly friend " + thief.name + " fudged");
            return;
        }
        var target_fish = msg.argcat(1);
        db.getFish(thief._id, function (thief_fish) {
            db.getFish(victim_id, function (victim_fish) {
                if (victim_fish.length >= TOO_MANY_FISH) {
                    sendChat("Friend " + victim.name + " is carrying too much.");
                    return;
                }
                if (thief_fish.length > 0) {
                    var idx = -1;
                    var arg = target_fish.trim().toLowerCase();
                    for (var i = 0; i < thief_fish.length; i++) {
                        if (arg == "" || thief_fish[i].toLowerCase().indexOf(arg) !== -1) {
                            idx = i;
                            break;
                        }
                    }
                    if (idx == -1) {
                        sendChat("Friend " + thief.name + ": You don't have " + arg + ".");
                        return;
                    }
                    var thefish = thief_fish[idx];
                    thief_fish.splice(idx, 1);
                    victim_fish.push(thefish);

                    sendChat("Our friend" + thief.name + " bestowed " + victim.name + " his/her " + thefish);
                    db.putFish(thief._id, thief_fish);
                    db.putFish(victim._id, victim_fish);
                } else {
                    sendChat("Friend " + thief.name + ": You don't have the fish to bestow.");
                }
            });
        });
        return;
    });

    // bot.addCommand(['/bestow_'], 0, msg => {

    // }, true);


}
