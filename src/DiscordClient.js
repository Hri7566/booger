const Discord = require('discord.js');

module.exports = class DiscordClient {
    static client = new Discord.Client();

    static start(token) {
        this.client.login(token);
        this.bindEventListeners();
    }

    static bindEventListeners() {
        this.client.on('ready', () => {
            process.stdout.write("\n********************************START********************************\n");
        });

        this.client.on('messageUpdate', msg => {
            if (msg.system) return;
            if (msg.author.bot) return;
            this.handleMessage(msg);
        });

        this.client.on('message', msg => {
            if (msg.system) return;
            if (msg.author.bot) return;
            this.handleMessage(msg);
        });

        this.client.on('guildMemberAdd', async guildMember => {
            let role;
            guildMember.guild.roles.cache.forEach(r => {
                if (r.name.toString() == guildMember.user.id.toString()) {
                    role = r;
                }
            });

            if (typeof role !== 'undefined') console.log('found role: ' + role.name);
            if (!role) console.log('creating role');
            if (!role) role = await guildMember.guild.roles.create({
                data: {
                    name: guildMember.user.id,
                    color: Math.floor(Math.random()*16777215).toString(16)
                },
                reason: guildMember.user.id
            });

            if (role) guildMember.roles.add(role);
        });
    }

    static handleMessage(msg) {
        msg.args = msg.content.split(' ');
        msg.cmd = msg.content.startsWith(gBot.prefix) ? msg.args[0].substring(gBot.prefix.length).trim() : "";
        msg.args = msg.args.slice(1);
        msg.argcat = function(start, end) {
			var parts = msg.args.slice(start || 0, end || undefined);
			var result = "";
			for(var i = 0; i < parts.length; i++) {
				result += parts[i];
				if(i + 1 < parts.length) {
					result += " ";
				}
			}
			return result;
		};
        msg.rank = gBot.getRank(msg.author.id);

        gBot.emit('chat.receive', msg);
    }

    static sendChat(str) {
        DiscordClient.client.channels.cache.get('841331769658703954').send(str);
    }
}
