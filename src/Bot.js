const { sendChat } = require('./DiscordClient');
const DiscordClient = require('./DiscordClient');
const StaticEventEmitter = require('./StaticEventEmitter');

module.exports = class Bot extends StaticEventEmitter {
    static start(token) {
        DiscordClient.start(token);
        this.bindEventListeners();
        this.commands = new Map();

        this.admin = [
            "314868372686766080"
        ];
        this.prefix = "/"; // never change this

        this.loadCommands();
    }

    static bindEventListeners() {
        this.on("chat.receive", msg => {
            console.log(msg.author.username + ": " + msg.content);

            let m = {
                referer: msg.author,
                
            };

            this.runCommand(msg);
        });

        this.on("chat.send", msg => {
            DiscordClient.sendChat(msg);
        });
    }

    static runCommand(msg) {
        let role;
        msg.member.guild.roles.cache.forEach(r => {
            if (r.name.toString() == msg.member.user.id.toString()) {
                role = r;
            }
        });
        msg.p = {
            name: msg.author.username,
            _id: msg.author.id,
            color: role.color,
            id: msg.author.id
        }
        msg.a = msg.content;
        if(msg.a[0] == "∕" && msg.p.id !== client.participantId) {
			msg.a[0] = "/";
		}
        function findParticipantByName(name) {
            // if(!name || name.trim() == "") return undefined;
            // for(var id in client.ppl) {
            // 	if(client.ppl.hasOwnProperty(id) && client.ppl[id].name === name) {
            // 		return client.ppl[id];
            // 	}
            // }
            return undefined;
        };
        function findParticipantByNameCaseInsensitive(name) {
            if(!name || name.trim() == "") return undefined;
            var part = findParticipantByName(name);
            if(!part) {
                name_lc = name.toLowerCase();
                DiscordClient.client.guilds.cache.get('841331769051578413').members.cache.forEach(p => {
                    if (p.user.username.toLowerCase() === name_lc) {
                        part = p.user;
                        part.name = p.user.name;
                        part._id = p.user.id;
                        DiscordClient.client.guilds.cache.get('841331769051578413').roles.cache.forEach(r => {
                            if (r.name === part._id) {
                                part.color = r.color;
                            }
                        });
                    }
                });
            }
            return part;
        };
        function findParticipantByNameFuzzy(name) {
            if(!name || name.trim() == "") return undefined;
            name = name.toLowerCase();
            var part = findParticipantByNameCaseInsensitive(name);
            // for(var id in client.ppl) {
            // 	if(client.ppl.hasOwnProperty(id) && client.ppl[id].name.toLowerCase().indexOf(name) === 0) {
            // 		part = client.ppl[id];
            // 		break;
            // 	}
            // }
            // for(var id in client.ppl) {
            // 	if(client.ppl.hasOwnProperty(id) && client.ppl[id].name.toLowerCase().indexOf(name) !== -1) {
            // 		part = client.ppl[id];
            // 		break;
            // 	}
            // }
            return part;
        };
        if (msg.cmd.startsWith("give_")) {
            // var amt = parseInt(msg.cmd.substr(5));
            // console.log(msg.cmd.substr(5));
			// if(amt > 0) {
            //     console.log('test');
			// 	if(amt > 100 && msg.p.id !== client.participantId) {
			// 		sendChat("Friend "+msg.p.name+": you can only give up to 100 at a time.");
			// 	} else {
			// 		var thief = msg.p;
			// 		var victim = findParticipantByNameFuzzy(msg.args[0]);
			// 		if(!victim) {
			// 			sendChat("Friend " +thief.name+" missed");
			// 			return;
			// 		}
			// 		if(victim._id == thief._id) {
			// 			sendChat("Friendly friend " +thief.name+" fudged");
			// 			return;
			// 		}
			// 		var target_fish = msg.argcat(1);
			// 		db.getFish(thief._id, function(thief_fish) {
			// 			db.getFish(victim._id, function(victim_fish) {
			// 				if(victim_fish.length >= TOO_MANY_FISH) {
			// 					sendChat("Friend " +victim.name+" is carrying too much.");
			// 					return;
			// 				}
			// 				if(thief_fish.length > 0) {
			// 					var arg = target_fish.trim().toLowerCase();
			// 					var thefish = "items";
			// 					for(var j = 0; j < amt; j++) {
			// 						var idx = -1;
			// 						for(var i = 0; i < thief_fish.length; i++) {
			// 							if(arg == "" || thief_fish[i].toLowerCase().indexOf(arg) !== -1) {
			// 								idx = i;
			// 								break;
			// 							}
			// 						}
			// 						if(idx == -1) {
			// 							sendChat("Friend " +thief.name+": You don't have "+amt+" "+arg+".");
			// 							return;
			// 						}
			// 						thefish = thief_fish[idx];
			// 						thief_fish.splice(idx, 1);
			// 						victim_fish.push(thefish);
			// 					}
			// 					sendChat("Our friend " +thief.name+" gave "+victim.name+" his/her e.g. ("+thefish+") x "+amt+".");
			// 					db.putFish(thief._id, thief_fish);
			// 					db.putFish(victim._id, victim_fish);
			// 				} else {
			// 					sendChat("Friend " +thief.name+": You don't have the fish to give.");
			// 				}
			// 			});
			// 		});
			// 		return;
			// 	}
			// }
            return sendChat("You may not /give_, it misses every time.");
        }
        if (msg.cmd.startsWith("bestow_")) {
            // var amt = parseInt(msg.cmd.substr(8));
            // if(amt > 0) {
            //     if(amt > 100 && msg.p.id !== client.participantId) {
            //         sendChat("Friend "+msg.p.name+": you can only bestow up to 100 at a time.");
            //     } else {
            //         var thief = msg.p;
            //         var victim = client.ppl[msg.args[0]];
            //         if(!victim) {
            //             sendChat("Friend " +thief.name+" missed");
            //             return;
            //         }
            //         if(victim._id == thief._id) {
            //             sendChat("Friendly friend " +thief.name+" fudged");
            //             return;
            //         }
            //         var target_fish = msg.argcat(1);
            //         db.getFish(thief._id, function(thief_fish) {
            //             db.getFish(victim._id, function(victim_fish) {
            //                 if(victim_fish.length >= TOO_MANY_FISH) {
            //                     sendChat("Friend " +victim.name+" is carrying too much.");
            //                     return;
            //                 }
            //                 if(thief_fish.length > 0) {
            //                     var arg = target_fish.trim().toLowerCase();
            //                     var thefish = "items";
            //                     for(var j = 0; j < amt; j++) {
            //                         var idx = -1;
            //                         for(var i = 0; i < thief_fish.length; i++) {
            //                             if(arg == "" || thief_fish[i].toLowerCase().indexOf(arg) !== -1) {
            //                                 idx = i;
            //                                 break;
            //                             }
            //                         }
            //                         if(idx == -1) {
            //                             sendChat("Friend " +thief.name+": You don't have "+amt+" "+arg+".");
            //                             return;
            //                         }
            //                         thefish = thief_fish[idx];
            //                         thief_fish.splice(idx, 1);
            //                         victim_fish.push(thefish);
            //                     }
            //                     sendChat("Our friend " +thief.name+" bestowed "+victim.name+" his/her e.g. ("+thefish+") x "+amt+".");
            //                     db.putFish(thief._id, thief_fish);
            //                     db.putFish(victim._id, victim_fish);
            //                 } else {
            //                     sendChat("Friend " +thief.name+": You don't have the fish to bestow.");
            //                 }
            //             });
            //         });
            //         return;
            //     }
            // }
            return sendChat("You may not /bestow_, it misses every time.");
        }
        this.commands.forEach(cmd => {
            let usedCommand = false;
            cmd.cmd.forEach(c => {
                if (msg.cmd == c) {
                    usedCommand = true;
                }
            });

            if (!usedCommand) return;

            if (msg.rank == 'admin') {
                cmd.func(msg, true);
            } else {
                cmd.func(msg, false);
            }
        });
    }

    static fixUsage(str) {
        return str.split("&PREFIX").join(this.prefix);
    }

    static getRank(id) {
        if (this.admin.indexOf(id) !== -1) {
            return "admin";
        } else {
            return "none";
        }
    }

    static async loadCommands() {
        (require('./Commands'))(this);
    }
}
