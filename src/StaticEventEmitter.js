module.exports = class StaticEventEmitter {
    static _events = {};

    static on(event, cb) {
        if (typeof this._events[event] == 'undefined') this._events[event] = [];
        this._events[event].push(cb);
    }

    static emit(evtn) {
        if(!this._events.hasOwnProperty(evtn)) return;
        var fns = this._events[evtn].slice(0);
        if(fns.length < 1) return;
        var args = Array.prototype.slice.call(arguments, 1);
        for(var i = 0; i < fns.length; i++) fns[i].apply(this, args);
    }
}