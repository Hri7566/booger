require('dotenv').config();

globalThis.gBot = require('./src/Bot');

const level = require('level');
const DiscordClient = require('./src/DiscordClient');

globalThis.db = level("./bot2019.db");

let sendChat = DiscordClient.sendChat;

db.getPokemon = function(id, cb) {
    var key = "pokemon collection~"+id;
    db.get(key, function(err, value) {
        if(err || !value || value == "") {
            cb([]);
            return;
        }
        var result = [];
        value = value.split("\xff");
        for(var i = 0; i < value.length; i++) {
            var v = value[i].trim();
            if(v.length) result.push(v);
        }
        cb(result);
    });
}

db.putPokemon = function(id, arr) {
    var result = "";
    for(var i = 0; i < arr.length; i++) {
        var v = arr[i];
        if(!v) continue;
        v = v.trim();
        if(v.length > 0) {
            if(i) result += "\xff";
            result += v;
        }
    }
    var key = "pokemon collection~"+id;
    if(result.length)
        db.put(key, result);
    else
        db.del(key);
}

db.readArray = function(start, end, cb) {
    var results = [];
    db.createReadStream({
        start: start,
        end: end
    })
    .on("data", function(data) {
        results.push(data);
    })
    .on("end", function() {
        cb(results);
    });
};

function listArray(arr) {
    var result = "";
    for(var i = 0; i < arr.length; i++) {
        if(i && i !== arr.length - 1) result += ", ";
        if(i && i === arr.length - 1) result += ", and ";
        result += arr[i];
    }
    return result;
}

// tries to find the thing by text
// calls cb with undefined or entry
db.look = function(location, text, cb) {
    text = text.toLowerCase().trim();
    if(text == "") {
        // "/look" with no search text
        db.get("look."+location, function(err, value) {
            var response = "";
            if(err) response = "Well...";
            else response = value;
            var sel = "look."+location+".◍";
            db.readArray(sel, sel+"\xff", function(results) {
                var results = results.map(data=>data.key.substr(sel.length));
                if(results.length) response += " There's "+listArray(results)+ ", about.";
                sendChat(response);
            });
        });
    } else {
        var entry = undefined;
        var sel = "look."+location+".";
        db.createReadStream({
            start: sel,
            end: sel+"◍\xff"
        })
        .on("data", function(data) {
            if(data.key.substr(sel.length).toLowerCase().indexOf(text) > -1) {
                entry = data;
            }
        })
        .on("end", function() {
            cb(entry);
        });
    }
}
db.take = function(location, text, cb) {
    text = text.toLowerCase().trim();
    var sel = "look."+location+".◍";
    var entry = undefined;
    db.createReadStream({
        start: sel,
        end: sel+"\xff"
    })
    .on("data", function(data) {
        if(data.key.substr(sel.length).toLowerCase().indexOf(text) > -1) {
            entry = data;
        }
    })
    .on("end", function() {
        cb(entry);
    });
}

db.getLocation = function(id, cb) {
    var key = "location~"+id;
    db.get(key, function(err, value) {
        if(err || !value || value == "") {
            return cb("outside");
        }
        return cb(value);
    });
}

db.setLocation = function(id, location) {
    if(!location || location === "") {
        location = "outside";
    }
    db.put("location~"+id, location);
}

db.getFish = function(id, cb) {
    var key = "fish sack~"+id;
    db.get(key, function(err, value) {
        if(err || !value || value == "") {
            cb([]);
            return;
        }
        var result = [];
        value = value.split("\xff");
        for(var i = 0; i < value.length; i++) {
            var v = value[i].trim();
            if(v.length) result.push(v);
        }
        cb(result);
    });
}

db.putFish = function(id, arr) {
    var result = "";
    for(var i = 0; i < arr.length; i++) {
        var v = arr[i];
        if(!v) continue;
        v = v.trim();
        if(v.length > 0) {
            if(i) result += "\xff";
            result += v;
        }
    }
    var key = "fish sack~"+id;
    if(result.length)
        db.put(key, result);
    else
        db.del(key);
}

db.appendFish = function(id, arr) {
    db.getFish(id, function(myfish) {
        myfish = myfish.concat(arr);
        //console.log(id, myfish);
        db.putFish(id, myfish);
    });
}

db.getFruits = function(cb) {
    var key = "kekklefruit tree";
    db.get(key, function(err, value) {
        if(err || !value || value == "") {
            cb(0);
            return;
        }
        cb(parseInt(value));
    });
}

db.setFruits = function(num_fruits) {
    var key = "kekklefruit tree";
    db.put(key, num_fruits);
}


gBot.start(process.env.DISCORD_TOKEN);
